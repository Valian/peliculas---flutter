import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:peliculas/src/models/actores_model.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class PeliculasProvider{
  String _apiKey = '10fc4570e2788989b73452713d08275f';
  String _languaje = 'es-ES';
  String _url = 'api.themoviedb.org';
  bool _cargando = false;

  int _popularesPage = 0;

  List<Pelicula> _populares = new List();

  final _popularesStreamController = StreamController<List<Pelicula>>.broadcast();

  Function(List<Pelicula>) get popularesSink =>_popularesStreamController.sink.add;

  Stream<List<Pelicula>> get popularesStream =>_popularesStreamController.stream;

  void disposeStreams(){
    _popularesStreamController?.close();
  }

Future<List<Pelicula>> _procesarRespuesta(url)async{
    final resp = await http.get( url );
    final decodedData = json.decode(resp.body);
    final peliculas = new Peliculas.fromJsonList(decodedData['results']);

    return peliculas.items;
}
Future<List<Pelicula>> getEnCines()async{

    final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key' : _apiKey,
      'language': _languaje
    });
    return await _procesarRespuesta(url);
  }
  Future<List<Pelicula>> getPopulares()async{

    if(_cargando) return [];

    _popularesPage++;

    final url = Uri.https(_url, '3/movie/popular', {

      'api_key' : _apiKey,
      'language': _languaje,
      'page'    : _popularesPage.toString()
    });

  final resp = await _procesarRespuesta(url);
    _populares.addAll(resp);
    popularesSink(_populares);
    _cargando = false ;
    return resp;
  }

  Future<List<Actor>> getCast(String peliId) async{
    final url = Uri.http(_url, '3/movie/$peliId/credits', {
      'api_key' : _apiKey,
      'language': _languaje,
    });

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);
    final casting = new Cast.fromJsonList(decodedData['cast']);
    return casting.actores;
  }

  Future<List<Pelicula>> buscarPelicula(String query)async{

    final url = Uri.https(_url, '3/search/movie', {
      'api_key' : _apiKey,
      'language': _languaje,
      'query'   : query
    });
    return await _procesarRespuesta(url);
  }
}