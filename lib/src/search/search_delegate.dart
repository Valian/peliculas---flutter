import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';
import 'package:peliculas/src/providers/peliculas_providers.dart';

class DataSearch extends SearchDelegate{

  final peliculasProvider = new PeliculasProvider();

  String _seleccion = '';

  final peliculas = [
    'Spiderman',
    'Aquaman',
    'Batman 1',
    'Batman 2',
    'Toy story',
    'Detective conan',
    'x-men',
    'Iron man',
  ];

  final peliculasRecientes = [
    'Spiderman',
    'Capitan america'
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    // acciones de nuestro appbar
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: (){
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Icono a la izquierda dle appbar
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: (){
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Crea los resultados a mostrar
    return Center(
      child: Container(
        height: 100,
        width: 100,
        color: Colors.blueAccent,
        child: Text(_seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // sugerencias que aparecen al escribir

  if( query.isEmpty ) return Container();


  return FutureBuilder(
    future: peliculasProvider.buscarPelicula(query),
    builder: (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
      if( snapshot.hasData ) {
        final peliculas = snapshot.data;
        return ListView(
          children: peliculas.map((pelicula){
            return ListTile(
              leading: FadeInImage(
                image: NetworkImage(pelicula.getPosterImg()),
                placeholder: AssetImage('assets/img/no-image.jpg'),
                width: 50,
                fit: BoxFit.contain,
              ),
              title: Text(pelicula.title),
              subtitle: Text(pelicula.originalTitle),
              onTap: (){
                close(context, null);
                pelicula.uniqueId = '';
                Navigator.pushNamed(context, 'detalle', arguments: pelicula);
              },
            );
          }).toList() // revisar
        );
      } else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    },
  );
  
  //   final listaSugerida = ( query.isEmpty ) 
  //                         ? peliculasRecientes 
  //                         : peliculas.where( (p)=> p.toLowerCase().contains(query)
  //                         ).toList();

  //   return ListView.builder(
  //     itemCount: listaSugerida.length,
  //     itemBuilder: (context, i){
  //       return ListTile(
  //         leading: Icon(Icons.movie),
  //         title: Text(listaSugerida[i]),
  //         onTap: (){
  //           _seleccion = listaSugerida[i];
  //           showResults(context);
  //         },
  //       );
  //     },
  //   );

  }

}